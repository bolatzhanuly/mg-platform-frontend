export const state = () => ({
  currentProductId: 0,

  // basket

  basketProducts: [],
  indeedBasketProducts: [],
  allQuantityOfBasketProducts: 0,
  allPriceOfBasketProducts: 0,

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Categories /////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  categories: [],
  currentCategoryId: 0,

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Products ///////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  products: [],

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Groups /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  groups: [],

})

export const mutations = {
  SET_CURRENT_PRODUCT (state, productId) {
    state.currentProductId = productId
  },

  // basket

  SET_CURRENT_BASKET_PRODUCTS (state, basketProducts) {
    state.basketProducts = basketProducts
  },
  SET_CURRENT_INDEED_BASKET_PRODUCTS (state, indeedBasketProducts) {
    state.indeedBasketProducts = indeedBasketProducts
  },
  ADD_ONE_PRODUCT_TO_BASKET (state, product) {
    state.basketProducts.push(product)
  },
  DELETE_ONE_PRODUCT_FROM_BASKET (state, productId) {
    let index = state.basketProducts.findIndex(product => product.id === productId)
    state.basketProducts.splice(index, 1)
  },
  INCREASE_TO_ONE_QUANTITY_OF_BASKET_PRODUCT (state, indeedBasketProductId) {
    let index = state.indeedBasketProducts.findIndex(indeedBasketProduct => indeedBasketProduct.id === indeedBasketProductId)
    state.indeedBasketProducts[index].quantity += 1
  },
  DECREASE_TO_ONE_QUANTITY_OF_BASKET_PRODUCT (state, indeedBasketProductId) {
    let index = state.indeedBasketProducts.findIndex(indeedBasketProduct => indeedBasketProduct.id === indeedBasketProductId)
    if (state.indeedBasketProducts[index].quantity >= 2) {
      state.indeedBasketProducts[index].quantity -= 1
    }
  },
  SET_CURRENT_QUANTITY_OF_BASKET_PRODUCTS (state) {
    let quantity = 0
    state.indeedBasketProducts.forEach(indeedBasketProduct => {
      quantity += indeedBasketProduct.quantity
    })
    state.allQuantityOfBasketProducts = quantity
  },
  SET_CURRENT_PRICE_OF_BASKET_PRODUCTS (state) {
    let price = 0
    let priceOfOneProduct = 0
    state.indeedBasketProducts.forEach(indeedBasketProduct => {
      state.basketProducts.forEach(basketProduct => {
        if (indeedBasketProduct.product === basketProduct.id) {
          priceOfOneProduct = indeedBasketProduct.quantity * basketProduct.price
          price += priceOfOneProduct
        }
      })
    })
    state.allPriceOfBasketProducts = price
  },
  DELETE_ONE_PRODUCT_FROM_INDEED_BASKET_PRODUCTS (state, productId) {
    let index = state.indeedBasketProducts.findIndex(indeedBasketProduct => indeedBasketProduct.product === productId)
    state.indeedBasketProducts.splice(index, 1)
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Categories /////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  SET_CATEGORIES (state, categories) {
    state.categories = categories
  },
  SET_CURRENT_CATEGORY (state, categoryId) {
    state.currentCategoryId = categoryId
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Products ///////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  SET_PRODUCTS (state, products) {
    state.products = products
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Groups /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  SET_GROUPS (state, groups) {
    state.groups = groups
  },
  ADD_GROUP (state, group) {
    console.log("group", group)
  }

}

export const actions = {
  async createCompany ({commit}, companyName) {
    await this.$axios.post('/companies/', {
      name: companyName
    })
    let companies = await this.$axios.get('/companies/')
    let companyId = companies.data[companies.data.length - 1].id
    await this.$axios.patch('/user/me/', {
      company: companyId
    })
    location.reload()
  },

  //basket

  async addProductToCart ({commit}, productId) {
    let products = await this.$axios.get('/products/')
    let cartProducts = await this.$axios.get('/cart/')
    products = products.data
    products = products.filter(product => product.id === productId)
    let productToAdd = products
    commit('ADD_ONE_PRODUCT_TO_BASKET', productToAdd)
    commit('SET_CURRENT_INDEED_BASKET_PRODUCTS', cartProducts)
  },
  async deleteProductFromCart ({commit}, productId) {
    let cartProducts = await this.$axios.get('/cart/')
    cartProducts = cartProducts.data
    cartProducts = cartProducts.filter(cartProduct => cartProduct.user === this.$auth.user.id && cartProduct.product === productId)
    let productToDelete = cartProducts
    await this.$axios.delete('/cart/' + productToDelete[0].id)
    commit('DELETE_ONE_PRODUCT_FROM_BASKET', productId)
    commit('DELETE_ONE_PRODUCT_FROM_INDEED_BASKET_PRODUCTS', productId)
    commit('SET_CURRENT_QUANTITY_OF_BASKET_PRODUCTS')
    commit('SET_CURRENT_PRICE_OF_BASKET_PRODUCTS')
  },
  async increaseQuantityToOne ({commit, getters}, indeedBasketProductId) {
    let products = getters.getIndeedBasketProducts
    let index = products.findIndex(indeedBasketProduct => indeedBasketProduct.id === indeedBasketProductId)
    let quantity = products[index].quantity
    await this.$axios.patch('/cart/' + products[index].id + "/", {
      quantity: quantity + 1
    })
    commit('INCREASE_TO_ONE_QUANTITY_OF_BASKET_PRODUCT', indeedBasketProductId)
    commit('SET_CURRENT_QUANTITY_OF_BASKET_PRODUCTS')
    commit('SET_CURRENT_PRICE_OF_BASKET_PRODUCTS')
  },
  async decreaseQuantityToOne ({commit, getters}, indeedBasketProductId) {
    let products = getters.getIndeedBasketProducts
    let index = products.findIndex(indeedBasketProduct => indeedBasketProduct.id === indeedBasketProductId)
    let quantity = products[index].quantity
    if (quantity >= 2) {
      await this.$axios.patch('/cart/' + products[index].id + "/", {
        quantity: quantity - 1
      })
    }
    commit('DECREASE_TO_ONE_QUANTITY_OF_BASKET_PRODUCT', indeedBasketProductId)
    commit('SET_CURRENT_QUANTITY_OF_BASKET_PRODUCTS')
    commit('SET_CURRENT_PRICE_OF_BASKET_PRODUCTS')
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Products ///////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  async fetchProducts ({commit}) {
    let products = await this.$axios.get('/products/')
    products = products.data
    commit('SET_PRODUCTS', products)
    return products
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Categories /////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  async fetchCategories ({commit}) {
    let categories = await this.$axios.get('/categories/')
    categories = categories.data
    commit('SET_CATEGORIES', categories)
    return categories
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Favorite Products //////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  async fetchFavoriteProducts () {
    let favoriteProducts = await this.$axios.get('/favorite_products/')
    favoriteProducts = favoriteProducts.data
    return favoriteProducts
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Groups /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  async fetchGroups ({commit}) {
    let groups = await this.$axios.get('/groups/')
    groups = groups.data
    groups = groups.filter(group => group.company === this.$auth.user.company)
    commit('SET_GROUPS', groups)
    return groups
  },
  async addGroup ({commit}, formDataGroup) {
    await this.$axios({
      method: 'POST',
      url: '/groups/',
      data: formDataGroup,
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    commit('ADD_GROUP', formDataGroup)
  },
  async deleteGroup ({}, groupId) {
    await this.$axios.delete('/groups/' + groupId + '/')
  }
}

export const getters = {
  getIndeedBasketProducts: (state) => {
    return state.indeedBasketProducts
  },

    ///////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////// Products ///////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  getProducts: (state) => {
    return state.products
  }
}
